# browser_hap

<img src="entry/src/main/resources/base/media/icon.png" width="64px" />

## 介绍

OpenHarmony浏览器 - OpenHarmony上一个包含展示网页，根据页面历史栈前进回退等基本功能的浏览器

参考仓库:

- applications_app_samples [Browser](https://gitee.com/openharmony/applications_app_samples/tree/master/code/BasicFeature/Web/Browser)

- OHOS Dev / [BrowserCE](https://gitee.com/ohos-dev/browser-ce)

## 开发环境

- DevEco Studio 3.1 Release
- SDK API10 4.0.7.2 Beta1 (目前验证browser可在API 9的机器上正常运行)

## 效果预览

| 首页                                       | 跳转                                       |
| ---------------------------------------- | ---------------------------------------- |
| ![home.png](screenshots/device/home.png) | ![jump.png](screenshots/device/jump.png) |
